package net.celisdelafuente.java.ocv.test;

import net.celisdelafuente.java.ocv.OcvImage;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ OcvImage.class })
public class AllTests {

}
